import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function App() {
  const [todos, setTodos] = useState([]);
  const [selectedOption, setSelectedOption] = useState('todos');

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(data => setTodos(data))
      .catch(error => console.error('Error fetching todos:', error));
  }, []);

  const filterTodos = () => {
    switch (selectedOption) {
      case 'todos':
        return todos.map(todo => ({ id: todo.id }));
      case 'pendingIds':
        return todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id }));
      case 'pendingList':
        return todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
      case 'pendingTitles':
        return todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
      case 'completedList':
        return todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
      case 'completedAndUserId':
        return todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
      case 'pendingAndUserId':
        return todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
      default:
        return todos;
    }
  };

  const renderItem = ({ item }) => (
    <View style={styles.todoItem}>
      <Text style={styles.todoTitle}>Título: {item.title}</Text>
      <Text>ID: {item.id}</Text>
      <Text>Usuario ID: {item.userId}</Text>
      <Text>Estado: {item.completed ? "Completado" : "No Completado"}</Text>
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.menu}>
        <TouchableOpacity onPress={() => setSelectedOption('todos')} style={selectedOption === 'todos' ? styles.selectedOption : styles.menuOption}>
          <Text style={styles.menuText}>Todos</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setSelectedOption('pendingIds')} style={selectedOption === 'pendingIds' ? styles.selectedOption : styles.menuOption}>
          <Text style={styles.menuText}>Pendientes (solo IDs)</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setSelectedOption('pendingList')} style={selectedOption === 'pendingList' ? styles.selectedOption : styles.menuOption}>
          <Text style={styles.menuText}>Pendientes (IDs y Títulos)</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setSelectedOption('pendingTitles')} style={selectedOption === 'pendingTitles' ? styles.selectedOption : styles.menuOption}>
          <Text style={styles.menuText}>Pendientes sin resolver (ID y Título)</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setSelectedOption('completedList')} style={selectedOption === 'completedList' ? styles.selectedOption : styles.menuOption}>
          <Text style={styles.menuText}>Completados (ID y Título)</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setSelectedOption('pendingAndUserId')} style={selectedOption === 'pendingAndUserId' ? styles.selectedOption : styles.menuOption}>
          <Text style={styles.menuText}>Pendientes (ID y Usuario ID)</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setSelectedOption('completedAndUserId')} style={selectedOption === 'completedAndUserId' ? styles.selectedOption : styles.menuOption}>
          <Text style={styles.menuText}>Completados (ID y Usuario ID)</Text>
        </TouchableOpacity>
      </View>
      <FlatList
        data={filterTodos()}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#fff',
  },
  menuOption: {
    paddingHorizontal: 10,
  },
  selectedOption: {
    paddingHorizontal: 10,
    borderBottomWidth: 2,
    borderBottomColor: 'blue',
  },
  menuText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333',
  },
  todoItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    backgroundColor: '#fff',
    width: '90%',
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 5,
  },
  todoTitle: {
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#333',
  },
});
