import * as React from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import MenuContainer from "../components/MenuContainer";
import ConfiguracionContainer from "../components/ConfiguracionContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";

const MenuAdministradores = () => {
  const navigation = useNavigation();
  const irASoporte = () => {
    navigation.navigate("SoporteTecnico");
  };
  const irAAjustes = () => {
    navigation.navigate("Ajustes");
  };
  const irAPerfil = () => {
    navigation.navigate("Perfil");
  };
  const irARutas = () => {
    navigation.navigate("Rutas");
  };
  const irACerrarSesion = () => {
    navigation.navigate("CerrarSesion");
  };
  const irAtras = () => {
    navigation.goBack();
  };
  const irRegistroEmpleados = () => {
    navigation.navigate("RegistroDeEmpleado");
  };
  const irRegistroConductor = () => {
    navigation.navigate("RegistroDeConductor");
  };
  const irRegistroTransporte = () => {
    navigation.navigate("RegistroDeTransporte");
  };
  const irCrearRuta = () => {
    navigation.navigate("CreacionDeRutas");
  };
  return (
    <View style={styles.menuAdministradores}>
      <View style={styles.topBar}>
        <TouchableOpacity onPress={irAtras}>
          <Image
            style={styles.icLeftIcon}
            source={require("../assets/icleft.png")}
          />
        </TouchableOpacity>
        <Text style={styles.title}>Menu</Text>
      </View>
      <View style={[styles.listSpaceBlock]}>
        <TouchableOpacity onPress={irAAjustes}>
          <ConfiguracionContainer
            iconEmoji="⚙️"
            menuOptionText="Configuración"
            propAlignSelf="stretch"
            propWidth="unset"
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irARutas}>
          <ConfiguracionContainer
            iconEmoji="🔍"
            menuOptionText="Ver rutas"
            propAlignSelf="stretch"
            propWidth="unset"
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irAPerfil}>
          <ConfiguracionContainer
            iconEmoji="👤"
            menuOptionText="Perfil"
            propAlignSelf="stretch"
            propWidth="unset"
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irRegistroEmpleados}>
          <ConfiguracionContainer
            iconEmoji="👨‍💼"
            menuOptionText="Registrar empleado"
            propAlignSelf="stretch"
            propWidth="unset"
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irRegistroConductor}>
          <ConfiguracionContainer
            iconEmoji="🚖"
            menuOptionText="Registrar conductor"
            propAlignSelf="stretch"
            propWidth="unset"
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irRegistroTransporte}>
          <ConfiguracionContainer
            iconEmoji="🚚"
            menuOptionText="Registrar transporte"
            propAlignSelf="stretch"
            propWidth="unset"
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irCrearRuta}>
          <ConfiguracionContainer
            iconEmoji="🗺️"
            menuOptionText="Crear ruta"
            propAlignSelf="stretch"
            propWidth="unset"
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irASoporte}>
          <ConfiguracionContainer
            iconEmoji="🗝️"
            menuOptionText="Soporte Tecnico "
            propAlignSelf="unset"
            propWidth={336}
            propColor="#000"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={irACerrarSesion}>
          <ConfiguracionContainer
            iconEmoji="🔒"
            menuOptionText="Cerrar sesión "
            propAlignSelf="unset"
            propWidth={336}
            propColor="#000"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    flexDirection: "row",
    paddingTop: Padding.p_base,
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignItems: "center",
  },
  list: {
    justifyContent: "center",
    paddingVertical: 0,
    paddingHorizontal: Padding.p_xs,
    marginTop: 12,
    alignItems: "center",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    flex: 1,
  },
  menuAdministradores: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    backgroundColor: Color.colorWhite,
    alignItems: "flex",
  },
  topBar: {
    flexDirection: "row",
    alignItems: "center",
    padding: Padding.p_5xs,
    backgroundColor: Color.colorWhite,
    elevation: 6,
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 6,
    shadowOpacity: 1,
  },
});

export default MenuAdministradores;
