import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import MissionContainer from "../components/MissionContainer";
import HomeBottomNavContainer from "../components/HomeBottomNavContainer";
import { FontSize, FontFamily, Color, Padding, Border } from "../GlobalStyles";
import { useNavigation } from "@react-navigation/native";

const AcercaDeNosotros = () => {
  const navigation = useNavigation();

  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.acercaDeNosotros}>
        <View style={styles.content}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Acerca de nosotros</Text>
        </View>
        <View style={[styles.list, styles.listSpaceBlock]}>
          <MissionContainer
            missionImageUrl={require("../assets/mision-1.png")}
            companyMissionDescription="Mision"
            transportServiceDescripti="Brindar un servicio para el transporte de personal eficiente, que cumpla las expectativas de los usuarios."
          />
          <MissionContainer
            missionImageUrl={require("../assets/vision-1.png")}
            companyMissionDescription="Visión"
            transportServiceDescripti="Nos comprometemos en brindar a nuestros clientes la mas alta eficiencia en: seguridad, puntualidad, amabilidad y confort para sus empleados"
            propMarginTop={8}
            propHeight={80}
          />
          <MissionContainer
            missionImageUrl={require("../assets/valores-1.png")}
            companyMissionDescription="Valores"
            transportServiceDescripti={`Ética
Respeto
Profesionalismo
Calidad
Honestidad`}
            propMarginTop={8}
            propHeight={100}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  listSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  topIcon: {
    maxWidth: "100%",
    overflow: "hidden",
    height: 24,
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    marginLeft: 8,
    flex: 1,
  },
  content: {
    flexDirection: "row",
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  list: {
    justifyContent: "center",
    paddingHorizontal: Padding.p_xs,
    paddingVertical: 0,
    alignItems: "center",
    verticalAlign: "auto",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    height: 273,
  },
  acercaDeNosotros: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default AcercaDeNosotros;
