import * as React from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import UserAvatarContainer from "../components/UserAvatarContainer";
import RealTimeLocationMapContainer from "../components/RealTimeLocationMapContainer";
import ListContainer from "../components/ListContainer";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";

const Principal = () => {
  const navigation = useNavigation();
  const irAMenuAdmin = () => {
    navigation.navigate("MenuAdministradores");
  };
  const irAPerfil = () => {
    navigation.navigate("Perfil");
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.principal}>
        <View style={[styles.topBar, styles.topBarShadowBox]}>
          <View style={styles.content}>
            <Text style={[styles.title, styles.titleTypo]}>
              Gestión del transporte
            </Text>
          </View>
        </View>
        <View style={styles.contentT}>
          <TouchableOpacity onPress={irAPerfil}>
            <UserAvatarContainer itemCode={require("../assets/avatar.png")} />
          </TouchableOpacity>
        </View>
        <RealTimeLocationMapContainer
          imageAltText={require("../assets/image.png")}
          locationDescription="Ubicación en tiempo real"
        />
        <View style={[styles.sectionTitle, styles.bottomNavSpaceBlock]}>
          <View style={styles.text}>
            <Text style={[styles.title1, styles.titleTypo]}>
              Información de transporte
            </Text>
          </View>
        </View>
        <ListContainer />
        <TouchableOpacity onPress={irAMenuAdmin} style={styles.bottomNavButton}>
          <View style={styles.bottomNav}>
            <View style={styles.tab}>
              <Image
                style={styles.image6Icon}
                contentFit="cover"
                source={require("../assets/image-6.png")}
              />
              <Text style={[styles.title3]} numberOfLines={1}>
                Menu
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  title3: {
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    textAlign: "center",
    display: "flex",
    height: 14,
    justifyContent: "center",
    color: Color.colorBlack,
    alignSelf: "stretch",
    alignItems: "center",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    flex: 1,
  },
  listSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  principal: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
  topBarShadowBox: {
    shadowOpacity: 1,
    elevation: 6,
    shadowRadius: 6,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowColor: "rgba(0, 0, 0, 0.12)",
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    color: Color.colorBlack,
  },
  bottomNavSpaceBlock: {
    marginTop: 12,
    flexDirection: "row",
  },
  tabFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  title: {
    fontSize: FontSize.size_xl,
    flex: 1,
  },
  content: {
    paddingLeft: Padding.p_base,
    paddingTop: Padding.p_xs,
    paddingRight: Padding.p_5xs,
    paddingBottom: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  contentT: {
    paddingRight: Padding.p_5xs,
    paddingBottom: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    alignSelf: "stretch",
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    alignSelf: "stretch",
    alignItems: "center",
  },
  image6Icon: {
    width: 21,
    height: 21,
  },
  title2: {
    fontSize: FontSize.size_3xs,
    lineHeight: 14,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
    textAlign: "center",
    justifyContent: "center",
    overflow: "hidden",
    alignSelf: "stretch",
  },
  tab: {
    padding: Padding.p_9xs,
    height: 53,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  bottomNavButton: {
    width: "auto",
    height: "auto",
    position: "absolute",
    bottom: 12,
    left: 0,
    right: 0,
    alignItems: "center",
  },
  bottomNav: {
    width: "auto",
    height: "auto",
    marginTop: 12,
    overflow: "hidden",
  },
});

export default Principal;
