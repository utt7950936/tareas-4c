import * as React from "react";
import { Image } from "expo-image";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import RealTimeLocationMapContainer from "../components/RealTimeLocationMapContainer";
import BusInfoContainer from "../components/BusInfoContainer";
import PasswordFormButtonContainer from "../components/PasswordFormButtonContainer";
import { FontFamily, Color, Padding, FontSize, Border } from "../GlobalStyles";

const CreacionDeRutas = () => {
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };

  return (
    <ScrollView style={styles.safeArea}>
      <View style={styles.creacionDeRutas}>
        <View style={styles.content}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={require("../assets/icleft.png")}
            />
          </TouchableOpacity>
          <Text style={[styles.title, styles.titleTypo]}>
            Creación de rutas
          </Text>
          <View style={styles.iconButtons} />
        </View>
        <View style={[styles.input, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Nombre de ruta</Text>
          <View style={[styles.textfield, styles.textfieldFlexBox]}>
            <Text style={[styles.text, styles.textTypo]} numberOfLines={1}>
              Ingresa nombre de la ruta
            </Text>
          </View>
        </View>
        <View style={[styles.selection, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Asignar transporte</Text>
          <View style={styles.textfieldFlexBox}>
            <View style={styles.chipFlexBox}>
              <Text style={[styles.text1, styles.textTypo]}>Torino</Text>
            </View>
            <View style={[styles.chip1, styles.chipFlexBox]}>
              <Text style={[styles.text1, styles.textTypo]}>Transporte B</Text>
            </View>
            <View style={[styles.chip1, styles.chipFlexBox]}>
              <Text style={[styles.text1, styles.textTypo]}>Transporte C</Text>
            </View>
          </View>
        </View>
        <View style={[styles.selection, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Conductor</Text>
          <View style={styles.textfieldFlexBox}>
            <View style={styles.chipFlexBox}>
              <Text style={[styles.text1, styles.textTypo]}>
                Valentina Campos
              </Text>
            </View>
            <View style={[styles.chip1, styles.chipFlexBox]}>
              <Text style={[styles.text1, styles.textTypo]}>Conductor B</Text>
            </View>
            <View style={[styles.chip1, styles.chipFlexBox]}>
              <Text style={[styles.text1, styles.textTypo]}>Conductor C</Text>
            </View>
          </View>
        </View>
        <View style={[styles.selection, styles.listSpaceBlock]}>
          <Text style={styles.title1}>Turno</Text>
          <View style={styles.textfieldFlexBox}>
            <View style={styles.chipLayout}>
              <Text style={[styles.text1, styles.textTypo]}>Matutino</Text>
            </View>
            <View style={[styles.chip7, styles.chipLayout]}>
              <Text style={[styles.text1, styles.textTypo]}>Vespertino</Text>
            </View>
            <View style={[styles.chip7, styles.chipLayout]}>
              <Text style={[styles.text1, styles.textTypo]}>Nocturno</Text>
            </View>
          </View>
        </View>
        <RealTimeLocationMapContainer
          imageAltText={require("../assets/image1.png")}
          locationDescription="Ruta en el mapa"
        />
        <View style={[styles.sectionTitle, styles.listSpaceBlock]}>
          <View style={styles.text10}>
            <Text style={[styles.title5, styles.titleTypo]}>Paradas</Text>
          </View>
        </View>
        <View style={[styles.list, styles.listSpaceBlock]}>
          <BusInfoContainer
            emojiIcon="📌"
            vehicleModelName="Parada 1"
            busStopInfo="Hora estimada:"
            busScheduleDetails="8:00 AM"
            busStopImageId={require("../assets/vector-2001.png")}
            propFontWeight="unset"
            propFontFamily="Roboto-Regular"
          />
          <BusInfoContainer
            emojiIcon="📌"
            vehicleModelName="Parada 2"
            busStopInfo="Hora estimada:"
            busScheduleDetails="9:00 AM"
            busStopImageId={require("../assets/vector-2001.png")}
            propFontWeight="unset"
            propFontFamily="Roboto-Regular"
          />
          <BusInfoContainer
            emojiIcon="📌"
            vehicleModelName="Parada 3"
            busStopInfo="Hora estimada:"
            busScheduleDetails="10:00 AM"
            busStopImageId={require("../assets/vector-2001.png")}
            propFontWeight="unset"
            propFontFamily="Roboto-Regular"
          />
          <BusInfoContainer
            emojiIcon="📌"
            vehicleModelName="Parada 4"
            busStopInfo="Hora estimada:"
            busScheduleDetails="11:00 AM"
            busStopImageId={require("../assets/vector-2001.png")}
            propFontWeight="unset"
            propFontFamily="Roboto-Regular"
          />
          <BusInfoContainer
            emojiIcon="📌"
            vehicleModelName="Parada 5"
            busStopInfo="Hora estimada:"
            busScheduleDetails="12:00 PM"
            busStopImageId={require("../assets/vector-2001.png")}
            propFontWeight="unset"
            propFontFamily="Roboto-Regular"
          />
          <BusInfoContainer
            emojiIcon="📌"
            vehicleModelName="Parada 6"
            busStopInfo="Hora estimada:"
            busScheduleDetails="1:00 PM"
            busStopImageId={require("../assets/vector-2001.png")}
            propFontWeight="unset"
            propFontFamily="Roboto-Regular"
          />
          <BusInfoContainer
            emojiIcon="📌"
            vehicleModelName="Parada 7"
            busStopInfo="Hora estimada:"
            busScheduleDetails="2:00 PM"
            busStopImageId={require("../assets/vector-2001.png")}
            propFontWeight="unset"
            propFontFamily="Roboto-Regular"
          />
        </View>
        <PasswordFormButtonContainer
          resetPasswordBtnText="Cancelar"
          actionButtonText="Guardar"
          propHeight="unset"
          propHeight1="unset"
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    color: Color.colorBlack,
  },
  listSpaceBlock: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    alignSelf: "stretch",
  },
  textfieldFlexBox: {
    marginTop: 4,
    flexDirection: "row",
    alignSelf: "stretch",
  },
  textTypo: {
    fontFamily: FontFamily.robotoRegular,
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  chipFlexBox: {
    padding: Padding.p_5xs,
    backgroundColor: Color.colorGray_400,
    borderRadius: Border.br_7xs,
    justifyContent: "center",
    alignItems: "center",
  },
  chipLayout: {
    width: 90,
    padding: Padding.p_5xs,
    backgroundColor: Color.colorGray_400,
    borderRadius: Border.br_7xs,
    justifyContent: "center",
    alignItems: "center",
  },
  topIcon: {
    maxWidth: "100%",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  iconButtons: {
    marginLeft: 8,
    alignItems: "center",
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  topBar: {
    shadowColor: "rgba(0, 0, 0, 0.12)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 6,
    elevation: 6,
    shadowOpacity: 1,
    alignSelf: "stretch",
    backgroundColor: Color.colorWhite,
  },
  title1: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    alignSelf: "stretch",
  },
  text: {
    color: Color.colorGray_500,
    height: 20,
    overflow: "hidden",
    flex: 1,
  },
  textfield: {
    borderStyle: "solid",
    borderColor: Color.colorGray_200,
    borderWidth: 1,
    paddingVertical: Padding.p_5xs,
    borderRadius: Border.br_7xs,
    marginTop: 4,
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
  input: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    justifyContent: "center",
    overflow: "hidden",
  },
  text1: {
    color: Color.colorBlack,
    fontFamily: FontFamily.robotoRegular,
  },
  chip1: {
    marginLeft: 8,
  },
  selection: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
    alignItems: "center",
  },
  chip7: {
    marginLeft: 8,
  },
  title5: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text10: {
    flex: 1,
  },
  sectionTitle: {
    paddingTop: Padding.p_base,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignItems: "center",
  },
  list: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    justifyContent: "center",
    alignItems: "center",
  },
  creacionDeRutas: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default CreacionDeRutas;
