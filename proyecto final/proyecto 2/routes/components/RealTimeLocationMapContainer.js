import * as React from "react";
import { Image } from "expo-image";
import { StyleSheet, Text, View} from "react-native";
import { FontSize, FontFamily, Color, Border, Padding } from "../GlobalStyles";

const RealTimeLocationMapContainer = ({
  imageAltText,
  locationDescription,
}) => {
  return (
    <View style={[styles.mapContainer, styles.imageIconLayout]}>
      <View style={styles.image}>
        <Image
          style={[styles.imageIcon, styles.iconPosition]}
          contentFit="fill"
          source={imageAltText}
        />
        <Text style={styles.title}>{locationDescription}</Text>
        <Image
          style={[styles.icLocationIcon, styles.iconPosition]}
          contentFit="fill"
          source={require("../assets/iclocation.png")}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imageIconLayout: {
    height: 336,
    overflow: "hidden",
  },
  iconPosition: {
    left: "50%",
    top: "50%",
    position: "absolute",
  },
  imageIcon: {
    marginTop: -168,
    marginLeft: -168,
    width: 336,
    overflow: "hidden",
    height: 336,
  },
  title: {
    marginTop: 8,
    left: 16,
    fontSize: FontSize.size_base,
    lineHeight: 22,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "center",
    width: 304,
    top: "50%",
    position: "absolute",
  },
  icLocationIcon: {
    marginTop: -24,
    marginLeft: -12,
    width: 24,
    height: 24,
  },
  image: {
    flex: 1,
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_400,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  mapContainer: {
    flexDirection: "row",
    paddingHorizontal: Padding.p_xs,
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
    alignSelf: "auto",
  },
});

export default RealTimeLocationMapContainer;
