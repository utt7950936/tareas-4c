import React, { useMemo } from "react";
import { Text, StyleSheet, View, ImageSourcePropType } from "react-native";
import { Image } from "expo-image";
import { FontFamily, Color, Border, Padding, FontSize } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const PropertyListContainer = ({
  propertyName,
  imageDimensions,
  propWidth,
  propWidth1,
  propWidth2,
}) => {
  const imageStyle = useMemo(() => {
    return {
      ...getStyleValue("width", propWidth),
    };
  }, [propWidth]);

  const title1Style = useMemo(() => {
    return {
      ...getStyleValue("width", propWidth1),
    };
  }, [propWidth1]);

  const image2IconStyle = useMemo(() => {
    return {
      ...getStyleValue("width", propWidth2),
    };
  }, [propWidth2]);

  return (
    <View style={[styles.list, styles.listFlexBox]}>
      <View style={[styles.list1, styles.listFlexBox]}>
        <View style={styles.row}>
          <View style={styles.cardLayout}>
            <View style={styles.textContent}>
              <Text style={styles.subtitle}>{propertyName}</Text>
            </View>
            <View style={[styles.image, imageStyle]}>
              <Text style={[styles.title, styles.textTypo, title1Style]}>
                Map of Route 1
              </Text>
              <Image
                style={[styles.image2Icon, image2IconStyle]}
                contentFit="cover"
                source={imageDimensions}
              />
            </View>
            <View style={styles.imageContainer} />
          </View>
          <View style={[styles.card1, styles.cardLayout]}>
            <View style={styles.textContent}>
              <Text style={styles.subtitle}>Horarios</Text>
            </View>
            <View style={styles.image}>
              <View style={[styles.chip, styles.chipLayout]}>
                <Text style={[styles.text, styles.textTypo]}>Matutino</Text>
              </View>
              <View style={[styles.chip1, styles.chipLayout]}>
                <Text style={[styles.text, styles.textTypo]}>Nocturno</Text>
              </View>
              <View style={[styles.chip2, styles.chipLayout]}>
                <Text style={[styles.text, styles.textTypo]}>Vespertino</Text>
              </View>
            </View>
            <View style={styles.imageContainer} />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listFlexBox: {
    justifyContent: "center",
    alignItems: "center",
  },
  textTypo: {
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
  },
  cardLayout: {
    height: 210,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderStyle: "solid",
    borderRadius: Border.br_7xs,
    width: 150,
    alignItems: "center",
    overflow: "hidden",
  },
  chipLayout: {
    width: 85,
    left: 35,
    position: "absolute",
    backgroundColor: Color.colorGray_400,
    padding: Padding.p_5xs,
    borderRadius: Border.br_7xs,
    justifyContent: "center",
    alignItems: "center",
  },
  subtitle: {
    fontSize: FontSize.size_base,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    textAlign: "left",
    color: Color.colorBlack,
    alignSelf: "stretch",
  },
  textContent: {
    padding: Padding.p_5xs,
    alignSelf: "stretch",
  },
  title: {
    marginTop: -8,
    top: "50%",
    left: 16,
    fontSize: FontSize.size_xs,
    lineHeight: 16,
    textAlign: "center",
    display: "flex",
    width: 116,
    height: 16,
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
  },
  image2Icon: {
    top: 0,
    left: 0,
    position: "absolute",
    height: 170,
    width: 150,
  },
  image: {
    width: 148,
    height: 170,
    backgroundColor: Color.colorGray_400,
  },
  imageContainer: {
    height: 150,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  text: {
    fontSize: FontSize.size_sm,
    lineHeight: 20,
    textAlign: "left",
  },
  chip: {
    top: 23,
  },
  chip1: {
    top: 111,
  },
  chip2: {
    top: 67,
  },
  card1: {
    marginLeft: 8,
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  list1: {
    width: 335,
    paddingLeft: Padding.p_xs,
    paddingRight: Padding.p_base,
    overflow: "hidden",
  },
  list: {
    paddingHorizontal: Padding.p_xs,
    paddingVertical: 0,
    marginTop: 12,
    overflow: "hidden",
    alignSelf: "stretch",
  },
});

export default PropertyListContainer;
