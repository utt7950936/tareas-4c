import React, { useMemo } from "react";
import { Image } from "expo-image";
import { StyleSheet, View, Text, ImageSourcePropType } from "react-native";
import { Color, FontSize, FontFamily, Padding } from "../GlobalStyles";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};
const MissionContainer = ({
  missionImageUrl,
  companyMissionDescription,
  transportServiceDescripti,
  image3Url,
  propMarginTop,
  propHeight,
}) => {
  const articleStyle = useMemo(() => {
    return {
      ...getStyleValue("marginTop", propMarginTop),
    };
  }, [propMarginTop]);

  const subtitleStyle = useMemo(() => {
    return {
      ...getStyleValue("height", propHeight),
    };
  }, [propHeight]);

  return (
    <View style={[styles.article, articleStyle]}>
      <View style={[styles.imageContainer, styles.mision1IconLayout]}>
        <Image
          style={styles.mision1IconLayout}
          contentFit="cover"
          source={missionImageUrl}
        />
      </View>
      <View style={styles.titleParent}>
        <Text style={[styles.title, styles.titleFlexBox]}>
          {companyMissionDescription}
        </Text>
        <Text
          style={[styles.subtitle, styles.titleFlexBox, subtitleStyle]}
          numberOfLines={image3Url}
        >
          {transportServiceDescripti}
        </Text>
      </View>
      <Image
        style={styles.articleChild}
        contentFit="cover"
        source={require("../assets/vector-2001.png")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  mision1IconLayout: {
    verticalAlign: "auto",
    height: 80,
    width: 80,
  },
  titleFlexBox: {
    textAlign: "left",
    color: Color.colorBlack,
    lineHeight: 20,
    alignSelf: "stretch",
  },
  imageContainer: {
    zIndex: 0,
    overflow: "hidden",
    flexDirection: "row",
  },
  title: {
    fontSize: FontSize.size_base,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
  },
  subtitle: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    height: 60,
    overflow: "hidden",
  },
  titleParent: {
    flex: 1,
    zIndex: 1,
    marginLeft: 12,
  },
  articleChild: {
    position: "absolute",
    right: 0,
    bottom: -1,
    left: 0,
    maxWidth: "100%",
    maxHeight: "100%",
    zIndex: 2,
    overflow: "hidden",
  },
  article: {
    justifyContent: "center",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_5xs,
    flexDirection: "row",
    alignSelf: "stretch",
  },
});

export default MissionContainer;
