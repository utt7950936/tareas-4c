import * as React from "react";
import { Text, StyleSheet, View } from "react-native";
import { Border, FontSize, FontFamily, Color, Padding } from "../GlobalStyles";

const ImageUploadContainer = () => {
  return (
    <View style={styles.imageContainer}>
      <View style={styles.image}>
        <Text style={[styles.title, styles.titleFlexBox]}>
          Subir captura de pantalla
        </Text>
        <View style={[styles.pagination, styles.titleFlexBox]}>
          <View style={[styles.paginationChild, styles.paginationLayout]} />
          <View style={[styles.paginationItem, styles.paginationLayout]} />
          <View style={[styles.paginationItem, styles.paginationLayout]} />
          <View style={[styles.paginationItem, styles.paginationLayout]} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  titleFlexBox: {
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
  },
  paginationLayout: {
    height: 4,
    borderRadius: Border.br_81xl,
  },
  title: {
    marginTop: -8,
    top: "50%",
    left: 16,
    fontSize: FontSize.size_base,
    lineHeight: 22,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "center",
    display: "flex",
    width: 304,
    height: 16,
  },
  paginationChild: {
    backgroundColor: Color.colorWhite,
    width: 20,
  },
  paginationItem: {
    backgroundColor: Color.colorGray_100,
    width: 4,
    marginLeft: 4,
  },
  pagination: {
    marginLeft: -22,
    bottom: 8,
    left: "50%",
    flexDirection: "row",
  },
  image: {
    flex: 1,
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_400,
    alignSelf: "stretch",
  },
  imageContainer: {
    height: 360,
    overflow: "hidden",
    paddingHorizontal: Padding.p_xs,
    paddingVertical: 0,
    marginTop: 12,
    flexDirection: "row",
    alignSelf: "stretch",
  },
});

export default ImageUploadContainer;
